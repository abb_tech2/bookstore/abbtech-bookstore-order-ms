package com.abbtech.bookstore_order_api.utils;

import com.abbtech.bookstore_order_api.model.Order;
import com.abbtech.bookstore_order_api.dto.request.OrderRequestDTO;
import com.abbtech.bookstore_order_api.dto.response.OrderResponseDTO;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class OrderConverter {

    public static Order toEntity(OrderRequestDTO orderRequest, UUID userId) {
        Order order = new Order();
        order.setBookId(orderRequest.bookId());
        order.setQuantity(orderRequest.quantity());
        order.setUserId(userId);
        return order;
    }

    public static OrderResponseDTO toDto(Order order) {
        return new OrderResponseDTO(order.getId(), order.getQuantity(), order.getUserId(), order.getBookId());
    }
}

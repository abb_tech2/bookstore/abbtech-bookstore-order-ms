package com.abbtech.bookstore_order_api.repository;

import com.abbtech.bookstore_order_api.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {
}

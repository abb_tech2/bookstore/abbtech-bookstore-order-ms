package com.abbtech.bookstore_order_api.dto.request;


import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record OrderRequestDTO(@NotNull UUID bookId,
                              @NotNull Integer quantity) {
}

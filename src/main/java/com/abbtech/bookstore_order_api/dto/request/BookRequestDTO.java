package com.abbtech.bookstore_order_api.dto.request;

import java.math.BigDecimal;
import java.util.UUID;

public record BookRequestDTO(
    UUID id,
    String title,
    String author,
    BigDecimal price
) {
}

package com.abbtech.bookstore_order_api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationReqDTO {
    private UUID orderId;
    private String userEmail;
    private UUID userId;
    private String message;
}

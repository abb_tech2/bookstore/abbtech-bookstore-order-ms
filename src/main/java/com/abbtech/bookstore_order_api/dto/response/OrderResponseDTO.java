package com.abbtech.bookstore_order_api.dto.response;

import java.util.UUID;

public record OrderResponseDTO(UUID id,
                               Integer quantity,
                               UUID userId,
                               UUID bookId) {
}

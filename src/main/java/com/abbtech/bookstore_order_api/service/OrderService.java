package com.abbtech.bookstore_order_api.service;

import com.abbtech.bookstore_order_api.dto.request.OrderRequestDTO;
import com.abbtech.bookstore_order_api.dto.response.OrderResponseDTO;

import java.util.UUID;

public interface OrderService {
    OrderResponseDTO create(OrderRequestDTO orderRequest, UUID userId);

    void confirm(String userEmail, UUID id);

    OrderResponseDTO getById(UUID id);
}

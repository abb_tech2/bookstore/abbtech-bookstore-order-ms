package com.abbtech.bookstore_order_api.service.impl;

import com.abbtech.bookstore_order_api.controller.BookClient;
import com.abbtech.bookstore_order_api.dto.request.NotificationReqDTO;
import com.abbtech.bookstore_order_api.model.Order;
import com.abbtech.bookstore_order_api.exception.BadRequestException;
import com.abbtech.bookstore_order_api.exception.enums.BadRequestExceptionEnum;
import com.abbtech.bookstore_order_api.dto.request.OrderRequestDTO;
import com.abbtech.bookstore_order_api.dto.response.OrderResponseDTO;
import com.abbtech.bookstore_order_api.repository.OrderRepository;
import com.abbtech.bookstore_order_api.service.OrderService;
import com.abbtech.bookstore_order_api.utils.OrderConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final BookClient bookClient;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public OrderResponseDTO create(OrderRequestDTO orderRequest, UUID userId) {
        log.info("Create order for user {}", userId);
        bookClient.getBookById(orderRequest.bookId());
        var order = OrderConverter.toEntity(orderRequest, userId);
        orderRepository.save(order);
        return OrderConverter.toDto(order);
    }

    public void confirm(String userEmail, UUID id) {
        log.info("Confirm order {} for user {}", id, userEmail);
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(BadRequestExceptionEnum.ORDER_NOT_FOUND));

        NotificationReqDTO notificationReqDTO = new NotificationReqDTO(id, userEmail, order.getUserId(), "Your order has been confirmed!");
        kafkaTemplate.send("order-confirmation", notificationReqDTO);
    }


    @Override
    public OrderResponseDTO getById(UUID id) {
        log.info("Get order by id {}", id);
        var order = orderRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(BadRequestExceptionEnum.ORDER_NOT_FOUND));
        return OrderConverter.toDto(order);
    }
}

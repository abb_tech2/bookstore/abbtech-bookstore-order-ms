package com.abbtech.bookstore_order_api.controller;

import com.abbtech.bookstore_order_api.dto.request.OrderRequestDTO;
import com.abbtech.bookstore_order_api.dto.response.OrderResponseDTO;
import com.abbtech.bookstore_order_api.exception.ErrorDetailDTO;
import com.abbtech.bookstore_order_api.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Validated
@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
@Tag(name = "Order Controller", description = "APIs for managing orders including creating, confirming, and retrieving order details.")
public class OrderController {

    private final OrderService orderService;

    @Operation(summary = "Create a new order", description = "Create a new order with the provided details and user ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order successfully created"),
            @ApiResponse(responseCode = "400", description = "Invalid request data",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class))),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class)))
    })
    @PostMapping
    public OrderResponseDTO create(@RequestHeader("X-USER-ID") UUID userId,
                                   @Valid @RequestBody OrderRequestDTO orderRequest) {
        return orderService.create(orderRequest, userId);
    }

    @Operation(summary = "Confirm an order", description = "Confirm an order with the provided user email and order ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order successfully confirmed"),
            @ApiResponse(responseCode = "404", description = "Order or user not found",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class)))
    })
    @PostMapping("/{id}")
    public void confirm(@RequestHeader("X-USER-EMAIL") String userEmail, @PathVariable UUID id) {
        orderService.confirm(userEmail, id);
    }

    @Operation(summary = "Get order by ID", description = "Retrieve details of an order by its unique ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved order details"),
            @ApiResponse(responseCode = "404", description = "Order not found",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class)))
    })
    @GetMapping("/{id}")
    public OrderResponseDTO get(@PathVariable("id") UUID id) {
        return orderService.getById(id);
    }
}

package com.abbtech.bookstore_order_api.controller;

import com.abbtech.bookstore_order_api.dto.request.BookRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;


@FeignClient(name = "ms-book", url = "${ms.book.url}")
@Service
public interface BookClient {

    @GetMapping("/{id}")
    BookRequestDTO getBookById(@PathVariable("id") UUID id);
}